require("dotenv").config();
const express = require("express");
const axios = require("axios");
const { exec } = require('child_process');

const app = express();
app.use(express.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Set-Cookie"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.options("*", (req, res) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Set-Cookie"
  );
  res.header("Access-Control-Allow-Credentials", "true");
  res.sendStatus(200);
});

const minioLoginUrl = `${process.env.MINIO_SERVER_API}/api/v1/login`;

app.post("/api/v1/login", async (req, res) => {
  try {
    const { accessKey, secretKey } = req.body;
    const response = await axios.post(minioLoginUrl, { accessKey, secretKey });
    const token = response.headers["set-cookie"];

    res.status(200).json({ token: token[0].substr(6) });
  } catch (error) {
    console.log(error.response.data.code)
    if(error.response.data.code === 401) {
      res
      .status(401)
      .json({ error: "Access denied: ", error: error.message });
    } else {
      res
      .status(500)
      .json({ error: "Something went wrong: ", error: error.message });
    }
  }
});

const minioApiUrl = `${process.env.MINIO_SERVER_API}`;

// Route to remove a user
app.post("/api/v1/remove-user", async (req, res) => {
  try {
    const { username } = req.body;
    console.log(username)
    // Execute the MinIO CLI command to remove the user using child_process
    exec(
      `mc admin user remove ${process.env.MINIO_NAME}/ ${username}`,
      (error, stdout, stderr) => {
        if (error) {
          console.error("Error removing user:", error.message);
          res
            .status(500)
            .json({ error: "Error removing user", details: error.message });
          return;
        }
        console.log("User removed:", stdout);
        res
          .status(200)
          .json({ message: `User '${username}' removed successfully!` });
      }
    );
  } catch (error) {
    console.error("Error removing user:", error.message);
    res
      .status(500)
      .json({ error: "Something went wrong: ", details: error.message });
  }
});

// Route to remove a policy
app.post("/api/v1/remove-policy", async (req, res) => {
  try {
    const { policyName } = req.body;
    console.log(policyName)
    // Execute the MinIO CLI command to remove the policy using child_process
    exec(
      `mc admin policy remove ${process.env.MINIO_NAME}/ ${policyName}`,
      (error, stdout, stderr) => {
        if (error) {
          console.error("Error removing policy:", error.message);
          res
            .status(500)
            .json({ error: "Error removing policy", details: error.message });
          return;
        }
        console.log("Policy removed:", stdout);
        res
          .status(200)
          .json({ message: `Policy '${policyName}' removed successfully!` });
      }
    );
  } catch (error) {
    console.error("Error removing policy:", error.message);
    res
      .status(500)
      .json({ error: "Something went wrong: ", details: error.message });
  }
});

app.all("/api/v1/*", async (req, res) => {
  try {
    const url = `${minioApiUrl}${req.originalUrl}`;

    const response = await axios({
      method: req.method,
      url: url,
      headers: {
        cookie: decodeURIComponent(req.headers.cookie),
      },
      data: req.body,
    });

    res.status(200).json(response.data);
  } catch (error) {
    console.log(error.response.data);
    res
      .status(error.response?.status || 500)
      .json({ error: "Something went wrong: ", error: error.response.data });
  }
});



app.listen(4000, () => {
  console.log("Proxy server is running on port 4000");
});
